# Spring Boot OAuth2 Authorization & Resource server (including RemoteTokenServices)

This is loosely based on [https://github.com/enr/spring-oauth-example] with the addition 
of a [RemoteTokenService](http://docs.spring.io/spring-security/oauth/apidocs/org/springframework/security/oauth2/provider/token/RemoteTokenServices.html)

## TODO
1. Run this on your terminal : `gradle build` to build all project.

2. Run shell script :
    * oauth-server : `run-oauth-server.sh`
    * resource-server : `run-resource-server.sh`
    * example-client : `run-html-server.sh`




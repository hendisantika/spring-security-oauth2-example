package com.hendisantika.example.oauth.resources;

import com.google.common.collect.ImmutableMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
/**
 * Created by IntelliJ IDEA.
 * Project : oauth-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/10/17
 * Time: 08.46
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/todos")
public class TodoResource {

    @RequestMapping(method = GET)
    public List<Map<String, Object>> getAll() {
        return asList(ImmutableMap.of("Hello", "World"));
    }
}

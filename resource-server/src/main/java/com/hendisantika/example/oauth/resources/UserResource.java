package com.hendisantika.example.oauth.resources;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
/**
 * Created by IntelliJ IDEA.
 * Project : oauth-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/10/17
 * Time: 08.49
 * To change this template use File | Settings | File Templates.
 */
@RestController
public class UserResource {

    @RequestMapping("/me")
    public Principal user(Principal user) {
        return user;
    }
}
